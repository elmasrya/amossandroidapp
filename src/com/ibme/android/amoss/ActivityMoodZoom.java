// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Originally written by Maxim Osipov

package com.ibme.android.amoss;

import com.ibme.android.amoss.R;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityMoodZoom extends ActivityWithMenu implements OnSharedPreferenceChangeListener {

	private static final String TAG = "AMoSSMoodZoom";

	private ClassMoodZoom mMoodZoom;

	public ActivityMoodZoom() {
		super(R.id.menu_mood_zoom);
	}
	
//	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	public void onCreate(Bundle savedInstanceState) {
//		if (BuildConfig.DEBUG) {
//			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//	                 .detectAll()
//	                 .penaltyLog()
//	                 .build());
//		}
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mood_zoom);

		// Start services
		Intent service = new Intent(this, ServiceCollect.class);
		this.startService(service);
		service = new Intent(this, ServiceUpload.class);
		this.startService(service);
		
		final long lMoodZoomType = getIntent().getLongExtra("typeMoodZoom", 0); // 0: disabled, 1: daily, 2: high intensity
		
		mMoodZoom = new ClassMoodZoom(this);

		final TextView textDescription = (TextView) findViewById(R.id.textViewDesc);
		if ((lMoodZoomType == 0) || (lMoodZoomType == 2)) {
			textDescription.setText(R.string.mood_zoom_text_description_current);
		}
		
		// Handle submit button
        final Button submit = (Button) findViewById(R.id.buttonSubmit);
        final RadioGroup radioMood_1 = (RadioGroup) findViewById(R.id.radioGroupMood_1);
        final RadioGroup radioMood_2 = (RadioGroup) findViewById(R.id.radioGroupMood_2);
        final RadioGroup radioMood_3 = (RadioGroup) findViewById(R.id.radioGroupMood_3);
        final RadioGroup radioMood_4 = (RadioGroup) findViewById(R.id.radioGroupMood_4);
        final RadioGroup radioMood_5 = (RadioGroup) findViewById(R.id.radioGroupMood_5);
        final RadioGroup radioMood_6 = (RadioGroup) findViewById(R.id.radioGroupMood_6);
        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	int mood_1_id = radioMood_1.getCheckedRadioButtonId();
            	int mood_2_id = radioMood_2.getCheckedRadioButtonId();
            	int mood_3_id = radioMood_3.getCheckedRadioButtonId();
            	int mood_4_id = radioMood_4.getCheckedRadioButtonId();
            	int mood_5_id = radioMood_5.getCheckedRadioButtonId();
            	int mood_6_id = radioMood_6.getCheckedRadioButtonId();

            	if ((mood_1_id == -1) || (mood_2_id == -1) || (mood_3_id == -1) ||
            			(mood_4_id == -1) || (mood_5_id == -1) || (mood_6_id == -1)) {
            		Toast.makeText(getBaseContext(), R.string.notification_mood_zoom_complete_all, Toast.LENGTH_SHORT).show();
            	} else {
	            	String mood_1 = ((RadioButton) findViewById(mood_1_id)).getText().toString();
	            	String mood_2 = ((RadioButton) findViewById(mood_2_id)).getText().toString();
	            	String mood_3 = ((RadioButton) findViewById(mood_3_id)).getText().toString();
	            	String mood_4 = ((RadioButton) findViewById(mood_4_id)).getText().toString();
	            	String mood_5 = ((RadioButton) findViewById(mood_5_id)).getText().toString();
	            	String mood_6 = ((RadioButton) findViewById(mood_6_id)).getText().toString();
	
	        		long ts = System.currentTimeMillis();
	        		mMoodZoom.init(ts);
	            	mMoodZoom.update(getBaseContext(), ts, mood_1, mood_2, mood_3, mood_4, mood_5, mood_6);
	            	Toast toast = Toast.makeText(getBaseContext(), R.string.notification_mood_zoom_submitted, Toast.LENGTH_SHORT);
	            	toast.show();
	            	
	            	if (lMoodZoomType > 0) {
		    			NotificationManager mNotificationManager =
		    				    (NotificationManager) ActivityMoodZoom.this.getSystemService(Context.NOTIFICATION_SERVICE);
		    			
		    			mNotificationManager.cancel(ClassConsts.NOTIFICATION_MOOD_ZOOM);
	            	}
	            	
	            	finish();
            	}
            }
        });
    }

	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this); 
		prefs.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this); 
		prefs.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onDestroy() {
		if (mMoodZoom != null) {
			mMoodZoom.fini();
		}
		new ClassEvents(TAG, "INFO", "Destroyed");
		super.onDestroy();
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
	}
}
