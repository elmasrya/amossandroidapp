// Original version Copyright (C) 2013, Maxim Osipov. All rights reserved.
//
// Modified version Copyright (c) 2014, Nick Palmius (AMoSS Study,
// University of Oxford). All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Original version by Maxim Osipov
// Forked from https://github.com/maximosipov/actopsy, commit id faa1a49996

package com.ibme.android.amoss;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.Toast;


public class TaskCheckerIBME extends AsyncTask<Void, Void, String> {

	private static final String TAG = "AMoSSTaskCheckerIBME";

	final Context context;
	private String mUserID;
	private String mUserPass;
	private String mServerRaw;
	private boolean mQuiet;

	TaskCheckerIBME(Context context) {
		this(context, false);
	}
	TaskCheckerIBME(Context context, boolean bQuiet) {
		this.context = context;
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		mUserID = prefs.getString("editUserID", "");
		mUserPass = prefs.getString("editUserPass", "");
		mServerRaw = prefs.getString("editServerRaw", "");
		mQuiet = bQuiet;
	}

	/*public class MyHttpClient extends DefaultHttpClient {

		final Context context;

		public MyHttpClient(Context context) {
			this.context = context;
		}

		@Override
		protected ClientConnectionManager createClientConnectionManager() {
			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", newSslSocketFactory(), 443));
			return new SingleClientConnManager(getParams(), registry);
		}

		private SSLSocketFactory newSslSocketFactory() {
			try {
				KeyStore trusted = KeyStore.getInstance("BKS");
				InputStream in = context.getResources().openRawResource(R.raw.ibmeweb7);
				try {
					trusted.load(in, "ez24get".toCharArray());
				} finally {
					in.close();
				}
				return new SSLSocketFactory(trusted);
			} catch (Exception e) {
				throw new AssertionError(e);
			}
		}
	}*/

	@Override
	protected String doInBackground(Void... nothing) {
		String result = "Unknown";
		try {
			// android.os.Debug.waitForDebugger();

			if (TextUtils.isEmpty(mUserID) || TextUtils.isEmpty(mUserPass) || TextUtils.isEmpty(mServerRaw)) {
				if (TextUtils.isEmpty(mServerRaw)) {
					result = "Server not set";
				} else {
					result = "Participant ID or password not set";
				}
				new ClassEvents(TAG, "ERROR", result);
				return result;
			}

			final String strUplaodPath = mServerRaw + ClassConsts.URL_CHECK;
			
			CredentialsProvider cp = new BasicCredentialsProvider();
			cp.setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
					new UsernamePasswordCredentials(mUserID, mUserPass));
			DefaultHttpClient httpclient = new DefaultHttpClient();
			httpclient.setCredentialsProvider(cp);
			HttpPost httppost = new HttpPost(strUplaodPath);

			// Prepare HTTP request
			MultipartEntity entity = new MultipartEntity();
			entity.addPart( "USER", new StringBody(mUserID));
			entity.addPart( "PASS", new StringBody(mUserPass));
			httppost.setEntity( entity );

			// Execute HTTP request
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity rsp = response.getEntity();
			if (rsp != null) {
				String str = EntityUtils.toString(rsp);
				if (str.matches("^OK(?s).*")) {
					result = "Connection Successful";
					new ClassEvents(TAG, "INFO", result);
				} else {
					result = "Connection error: " + str;
					new ClassEvents(TAG, "ERROR", result);
				}
			}
			
		} catch (Exception e) {
			result = "Connection failed: " + e.getMessage();
			new ClassEvents(TAG, "ERROR", result);
		}

		return result;
	}

	protected void onPostExecute(String result){
		if (!mQuiet) {
	    	Toast toast = Toast.makeText(context, result, Toast.LENGTH_SHORT);
	    	toast.setGravity(Gravity.CENTER, 0, 0);
	    	toast.show();
		}
	}
}
