// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Based on code by Maxim Osipov (https://github.com/maximosipov/actopsy)

package com.ibme.android.amoss;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

public class ClassReporting {

	private static final String TAG = "AMoSSReporting";

	public ClassReporting(String setting, String value)
	{
		this(setting, value, null, false);
	}

	public ClassReporting(String setting, String value, Context context)
	{
		this(setting, value, context, true);
	}
	
	public ClassReporting(String setting, String value, Context context, boolean uploadNow)
	{
		try {
			long ts = System.currentTimeMillis();
			File root = Environment.getExternalStorageDirectory();
			if (root.canWrite()){
				// Create folders
				File folder = new File(root, ClassConsts.FILES_ROOT + ClassConsts.REPORTING_SUB_DIR);
				if (!folder.exists()) {
					folder.mkdirs();
				}
				// Initialise events file
				File out = new File(folder, "reporting.dat");
				BufferedWriter writer = new BufferedWriter(new FileWriter(out, true));
				// Write data and close events file 
				SimpleDateFormat fmt = new SimpleDateFormat(ClassConsts.DATEFMT);
				Log.d(TAG, setting + "," +  value + "\n");
				writer.write(fmt.format(new Date(ts)) + "," + setting + "," + value + "\n");
				writer.close();
				
				if (uploadNow) {
					fmt = new SimpleDateFormat("yyyyMMddHHmmssSSS");
					File outUpload;
					int i = 0;
					do {
						i++;
						outUpload = new File(folder, "reporting." + fmt.format(new Date(ts)) + "." + String.format("%03d", i) +".upload.dat");
					} while (outUpload.exists());
					
					out.renameTo(outUpload);
					
					File[] files = folder.listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							return name.toLowerCase().matches(".*.upload.dat$");
						}
					});

					ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
					NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
					if (ni != null && ni.isConnected()) {
						Log.i(TAG, "Starting upload of " + files.length + " files");
						new TaskUploaderIBME(context, "text/csv", "reporting.dat").execute(files);
					}
				}
			} else {
				Log.e(TAG, "SD card not writable");
			}
		} catch (Exception e) {
			Log.e(TAG, "Could not write file: " + e.getMessage());
		}
	}
}
