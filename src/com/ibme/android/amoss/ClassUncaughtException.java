// Copyright (c) 2014, Nick Palmius (AMoSS Study, University of Oxford).
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the name of the University of Oxford nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Contact: Nick Palmius (npalmius@googlemail.com)

// Based on code by Maxim Osipov (https://github.com/maximosipov/actopsy)

package com.ibme.android.amoss;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;

public class ClassUncaughtException implements java.lang.Thread.UncaughtExceptionHandler {

	private static final String TAG = "AMoSSUncaughtException";
	
	private Thread.UncaughtExceptionHandler androidDefaultUEH;
	
	public ClassUncaughtException()
	{
	    androidDefaultUEH = Thread.getDefaultUncaughtExceptionHandler();
	    Thread.setDefaultUncaughtExceptionHandler(this);
	}
	
	@Override
	public void uncaughtException(Thread thread, Throwable exception) {
		try {
	        StringWriter stackTrace = new StringWriter();
	        exception.printStackTrace(new PrintWriter(stackTrace));
	        
			long ts = System.currentTimeMillis();
			File root = Environment.getExternalStorageDirectory();
			if (root.canWrite()){
				// Create folders
				File folder = new File(root, ClassConsts.FILES_ROOT);
				if (!folder.exists()) {
					folder.mkdirs();
				}
				String dateTimeString = DateFormat.format(ClassConsts.EXCEPTION_FILE_DATE_FORMAT, System.currentTimeMillis()).toString();
				// Initialise events file
				File out = new File(folder, "exception-" + dateTimeString + ".dat");
				BufferedWriter writer = new BufferedWriter(new FileWriter(out, true));
				// Write data and close events file 
				SimpleDateFormat fmt = new SimpleDateFormat(ClassConsts.DATEFMT);
				writer.write(fmt.format(new Date(ts)) + "\n");
				writer.write(stackTrace.toString());
				writer.close();
				
				TaskZipper.zipFiles(out);
				
			} else {
				Log.e(TAG, "SD card not writable");
			}
		} catch (Exception e) {
			Log.e(TAG, "Could not write file: " + e.getMessage());
		}
        
		androidDefaultUEH.uncaughtException(thread, exception);
	}
	
	public static void AttemptZipUploadExceptions(Context context)
	{
		File root = Environment.getExternalStorageDirectory();
		File folder = new File(root, ClassConsts.FILES_ROOT);
		File[] files;
		
		if (folder.exists()) {
			files = folder.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.toLowerCase().matches("exception-.*dat$");
				}
			});
			if ((files != null) && (files.length > 0)) {
				TaskZipper.zipFiles(files);
			}
			
			ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo ni = connectivityManager.getActiveNetworkInfo();
			if (ni != null && ni.isConnected()) {
				files = folder.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return name.toLowerCase().matches("exception-.*zip$");
					}
				});
				if ((files != null) && (files.length > 0)) {
					TaskUploaderIBME uploader = new TaskUploaderIBME(context);
					uploader.execute(files);
					/*try {
						uploader.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}*/
				}
			}
		}
	}
}
